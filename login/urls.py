"""
CREAT: 2017/10/14
AUTHOR:　HEHAHUTU
"""
from django.conf.urls import url, include
from login import views
urlpatterns = [
    url(r'^login/$', view=views.login, name='login'),
    url(r'^logout/$', view=views.logout, name='logout'),
    url(r'^check/$', view=views.check_user_test, name='check')
]