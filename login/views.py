# Create your views here.
from django.contrib.auth.models import User
from django.shortcuts import render
from django.http.response import JsonResponse
from django.contrib.auth.hashers import check_password
import json
from login.models.token import Token
import uuid
import pytz
from datetime import datetime, timedelta, timezone
from login.auth_user import check_token, check_login_token
from django.conf import settings


# Create your views here.

def json_renderer(data):
    return json.loads(data)


@check_login_token
def login(request):
    if request.method == 'POST':
        # 接收json 参数
        da = request.body.decode()
        data = json_renderer(da)
        # 备用接收post参数数据方式
        new_da = request.POST.get('username')
        print(request.COOKIES.get('token'))
        print(new_da)
        try:
            username = data['username']
            password = data['password']
            print(username, password)
            check = User.objects.get(username__exact=username)

            print(check.password)
            if check_password(password, check.password):
                token_key = uuid.uuid4().hex
                user_id = check.id
                pz = pytz.timezone('Asia/Shanghai')

                expire_date = datetime.now(pz) + timedelta(hours=settings.EXPIRE_DATE)
                print(token_key, user_id, expire_date)
                Token.objects.create(token_key=token_key, token_value=username, user_id=user_id,
                                     expire_date=expire_date)
                resp = JsonResponse({'status': 'ok', 'token': token_key})
                resp.set_cookie('token', token_key)
                return resp
            else:
                return JsonResponse({'status': 'Exception', 'msg': 'password error'})
        except Exception as e:
            # print(e)
            return JsonResponse({'status': 'Exception', 'msg': str(e)})
    elif request.method == 'GET':
        username = request.GET.get('username')
        password = request.GET.get('password')
        if username and password:
            check = User.objects.get(username__exact=username)

            print(check.password)
            if check_password(password, check.password):
                token_key = uuid.uuid4().hex
                user_id = check.id
                pz = pytz.timezone('Asia/Shanghai')

                expire_date = datetime.now(pz) + timedelta(days=1)
                print(token_key, user_id, expire_date)
                Token.objects.create(token_key=token_key, token_value=username, user_id=user_id,
                                     expire_date=expire_date)
                resp = JsonResponse({'status': 'ok', 'token': token_key})
                resp.set_cookie('token', token_key)
                return resp
            else:
                return JsonResponse({'status': 'Exception', 'msg': 'password error'})
        else:
            return JsonResponse({'status': 'Exception', 'msg': 'check username or password'})


@check_token
def logout(request):
    token = request.COOKIES.get('token')
    try:
        Token.objects.get(token_key=token).delete()
        return JsonResponse({'status': 'ok', 'token': token})
    except Exception as e:
        return JsonResponse({'status': 'Exception', 'msg': str(e)})


@check_token
def check_user_test(request):
    print(request.body)
    return JsonResponse({'status': 'ok'})
