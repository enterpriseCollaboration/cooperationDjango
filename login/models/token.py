"""
CREAT: 2017/10/14
AUTHOR:　HEHAHUTU
"""
from django.db import models


class Token(models.Model):
    token_key = models.CharField('token', max_length=100)
    token_value = models.CharField('username', max_length=20)
    user_id = models.IntegerField('user id')
    expire_date = models.DateTimeField()

    def __str__(self):
        print(f'token: {self.token_key} expire data: {self.expire_date}')
