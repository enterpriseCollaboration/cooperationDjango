from django.test import TestCase
import requests

# Create your tests here.

header = {
    'Cookie': 'csrftoken=WO8cPtXXOt1wT3c0forWJZEuiNCDSH1ZCA9Py5oixbASvorIlYUEMAKLsu4tGgol;token=ecfb7b34e973443cb8a1ad00b0fc3065',
}


def test_login():
    html = requests.post('http://127.0.0.1:8000/api/login/', json={'username': 'admin', 'password': 'test1234'},
                         headers=header)
    print(html.content.decode())


def test_chekc_token():
    html = requests.post('http://127.0.0.1:8000/api/check/', json={'username': 'admin', 'password': 'test1234'},
                         headers=header)
    print(html.content.decode())


def test_logout():
    html = requests.post('http://127.0.0.1:8000/api/logout/', json={'username': 'admin', 'password': 'test1234'},
                         headers=header)
    print(html.content.decode())


if __name__ == '__main__':
    test_logout()
